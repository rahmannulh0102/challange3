package challange2;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class read {
    private String path;
    public read(String path) {
        this.path = path;
    }
    public ArrayList<Integer> NilaiKelas()
    {
        String line = "";
        String separator = ";";
        BufferedReader br = null;
        String angka;
        ArrayList<Integer> nilai = new ArrayList<Integer>();

        try {
            br = new BufferedReader(new FileReader(this.path));
            while ((line = br.readLine()) != null) {
                String[] parsingFile = line.split(separator);
                for (int x = 1; x < parsingFile.length; x++) {
                    angka = parsingFile[x];
                    nilai.add(Integer.parseInt(angka));
                }
            }
        } catch (FileNotFoundException ex) {
            System.err.println("File tidak ditemukan ");
            ex.printStackTrace();
        } catch (IOException ex) {
            System.err.println("File tidak ditemukan ");
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return nilai;
    }
}
