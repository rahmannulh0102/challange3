package challange2;

import java.util.ArrayList;
import java.util.Scanner;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class main {
    public static void main(String[] args) {
        String path = "D:/kuliah/challange/data_sekolah.csv";
        read baca = new read(path);
        ArrayList<Integer> nilai = baca.NilaiKelas();
        math Math = new math();
        float rata = Math.rata(nilai);
        int median = Math.median(nilai);
        int modus = Math.modus(nilai);

        Scanner input = new Scanner(System.in);
        System.out.println("---------------------------------------------------");
        System.out.println("Aplikasi pengolah nilai siswa");
        System.out.println("---------------------------------------------------");
        System.out.println("letakkan file csv dengan nama file data_sekolah di");
        System.out.println("direktori berikut: D:/kuliah/challange/");
        System.out.println(" ");
        System.out.println("Menu:");
        System.out.println("1. generate txt untuk menampilkan modus ");
        System.out.println("2. generate txt untuk menampilkan mean dan median ");
        System.out.println("3. generate txt untuk menampilkan kedua file ");
        System.out.println("0. exit ");
        System.out.println("pilih menu:");
        int menu = input.nextInt();
        if(menu == 1){
            menu1(modus);
        }
        if(menu == 2){
            menu2(modus,rata,median);
        }
        if(menu == 3){
            menu1(modus);
            menu2(modus,rata,median);
        }

    }
    public static void menu1(int modus){
        try {
            FileWriter writer = new FileWriter("D:/kuliah/challange/modus.txt", true);
            BufferedWriter bufferedWriter = new BufferedWriter(writer);

            bufferedWriter.write("Berikut Hasil Pengolahan Nilai:");
            bufferedWriter.newLine();
            bufferedWriter.write("nilai modus = "+modus);
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("data sudah di simpan di (D:/kuliah/challange/modus.txt)");
    }
    public static void menu2(int modus,float rata,int median){
        try {
            FileWriter writer = new FileWriter("D:/kuliah/challange/sebaran data.txt", true);
            BufferedWriter bufferedWriter = new BufferedWriter(writer);

            bufferedWriter.write("Berikut Hasil Pengolahan Nilai:");
            bufferedWriter.newLine();
            bufferedWriter.write("Berikut Hasil sebaran data");
            bufferedWriter.newLine();
            bufferedWriter.write("mean = "+rata);
            bufferedWriter.newLine();
            bufferedWriter.write("median = "+median);
            bufferedWriter.newLine();
            bufferedWriter.write("modus = "+ modus);
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("data sudah di simpan di (D:/kuliah/challange/sebaran data.txt)");
    }
}
